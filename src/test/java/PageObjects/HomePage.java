package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import stepDefinations.TestBase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class HomePage extends TestBase {
    By labelHomePage;
    By nabuLabelHomePage;
    By dismissButton;
    By modelDialog;
    By viewMyActivityLink;
    By userAccountInfoLink;
    By viewAccountInfo;
    By logoutLink;
    By userAccountInformationStatusCheck;
    By dashboardContentVersionBarDescriptionTitle;
    By ColumnDateLabel;
    By ColumnPageLabel;
    By ColumnSectionLabel;
    By myVieweingActivitySection;
    By myVieweingActivityDates;
    By menubarlist;
    By navigationItemList;
    By loadingElement;
    By navigationItemElement;
    By textInfo;
    By bookmarkDateColumnName;
    By bookmarkSectionColumnName;
    By bookmarksSection;
    By dateSort;
    By pageSort;
    By sectionSort;
    By myVieweingActivityPages;
    By myVieweingActivitySections;

    public HomePage() {
        InitElements();
    }

    private void InitElements() {
        labelHomePage = By.xpath("//div[@class='dashboard-content-title']");
        dismissButton = By.xpath("//button[text()='Dismiss']");
        modelDialog = By.xpath("//div[@class='modal-dialog modal-dialog']");
        viewMyActivityLink = By.xpath("//div[@aria-label='My Viewing Activity']");
        userAccountInfoLink = By.xpath("//span[@aria-label='User Account Information']");
        viewAccountInfo = By.xpath("//a[@class='btn user-popover-button ']//span[text()='View Account Info']");
        logoutLink = By.xpath("//a[@class='btn user-popover-button ']//span[text()='Logout']");
        userAccountInformationStatusCheck = By.xpath("//span[@aria-label='User Account Information']//span[2]");
        dashboardContentVersionBarDescriptionTitle = By.xpath("//div[@class='dashboard-content-version-bar-description-title']");
        ColumnDateLabel = By.xpath("//span[@aria-label='Column Date']");
        ColumnPageLabel = By.xpath("//span[@aria-label='Column Page']");
        ColumnSectionLabel = By.xpath("//span[@aria-label='Column Section']");
        myVieweingActivitySection = By.xpath("//div[@aria-label='My Viewing Activity']");
        bookmarksSection = By.xpath("//div[@aria-label='Bookmarks']");
        myVieweingActivityDates = By.xpath("//td[@class='cell-date']//span");
        myVieweingActivityPages = By.xpath("//td[@class='cell-page']//span");
        myVieweingActivitySections = By.xpath("//td[@class='cell-section']//span");
        nabuLabelHomePage = By.xpath("//h1[text()='Food Chemicals Codex (FCC)']");
        menubarlist= By.xpath("//ul[@class='usp-menu-bar']//li");
        navigationItemList = By.xpath("//a[@class='navigation-item-link']");
        loadingElement = By.xpath("//h1[@class='section-index-page-title'][contains(text(),'Loading')]");
        textInfo = By.xpath("//div[@class='dashboard-content-body']//div");
        bookmarkDateColumnName = By.xpath("//span[@aria-label='Column Date']");
        bookmarkSectionColumnName =  By.xpath("//span[@aria-label='Column Section']");
        dateSort =  By.xpath("//*[@aria-label='Sort Column Date']//span[contains(@class,'multi-arrow')]");
        pageSort =  By.xpath("//*[@aria-label='Sort Column Page']//span[contains(@class,'multi-arrow')]");
        sectionSort =  By.xpath("//*[@aria-label='Sort Column Section']//span[contains(@class,'multi-arrow')]");
    }

    public void closeNoticeDialogs() throws Exception {
        pause(3000);
        for (int i = 0; i < 3; i++) {
            List<WebElement> list=driver.findElements(modelDialog);
            if (list.size() > 0) {
                logger.info("Closing Notice Dialog");
                assert click(dismissButton);
            }
        }
    }

    public void verifyHomePageDisplayed() throws Exception {
        try {
            waitForElementToDisplay(labelHomePage, 60);
            assert isElementCurrentlyDisplayed(labelHomePage);
            test.pass("Home page is displayed");
        } catch (Exception e) {
            test.fail("Home page is not displayed");
        }
    }

    public void verifyNabuHomePageDisplayed(){
        try {
            waitForElementToDisplay(labelHomePage, 5);
            assert isElementCurrentlyDisplayed(nabuLabelHomePage);
            test.pass("Login successfull and Home page is displayed");
        } catch (Exception e) {
            test.fail("Login not successfull "+e);
        }
    }

    public void setViewMyActivityLink() throws Exception {
        assert  click(viewMyActivityLink);
    }

    public void navigateToViewAccountInfoSection() throws Exception {
        assert click(viewAccountInfo);
    }

    public void ExpandUserAccountInfoLink() throws Exception {
        if (getAttributeValueFromElement(userAccountInformationStatusCheck, "class").contains("icon icon-white-down-arrow")) {
            assert click(userAccountInfoLink);
            logger.info("User Account Info is expanded");
        } else {
            logger.info("User Account Info is already expanded");
        }
    }

    public boolean verifyUserAccountInfoLinkDisplayed() throws Exception {
        return isElementCurrentlyDisplayed(userAccountInformationStatusCheck);
    }

    public void logoutFromApplication() throws Exception {
        assert click(logoutLink);
    }

    public void switcToHomePage() {
        driver.switchTo().window(world.get("mainWin"));
    }

    public void verifyOfficialCurrentVersionLabel() throws Exception {
        waitUntilElementIsVisible(dashboardContentVersionBarDescriptionTitle,20);
        String actValue= getTextFromElement(dashboardContentVersionBarDescriptionTitle);
        Assert.assertTrue("", actValue.equalsIgnoreCase("Currently Official"));
    }

    public void verifyMyViewingActivitySection() throws Exception {
        try {
            assert isElementCurrentlyDisplayed(myVieweingActivitySection);
            moveToElement(myVieweingActivitySection);
            test.pass("My Viewing Activity section is selected");
        } catch (Exception e) {
            test.fail("My Viewing Activity section is not selected");
        }
    }

    public void clickBookmarksTab() throws Exception {
        assert click(bookmarksSection);
    }

    public void verifyPublicationsSortedByDescendingOrder() throws ParseException, InterruptedException {
        boolean flag = false;
        List<String> dates = getDatesList();
        for (int i = 0; i < dates.size() - 1; i++) {
            if (compareDates(dates.get(i), dates.get(i + 1)) < 0) {
                flag = true;
            }
        }
        if (flag == false) {
            test.pass("Dates are sorted in descending order by date");
        } else {
            test.fail("Dates are not sorted in descending order by date");
        }
    }

    public void verifyPublicationsSortedByAscOrder() throws ParseException, InterruptedException {
        boolean flag = false;
        List<String> dates = getDatesList();
        for (int i = 0; i < dates.size() - 1; i++) {
            if (compareDates(dates.get(i), dates.get(i + 1)) > 0) {
                flag = true;
            }
        }
        if (flag == false) {
            test.pass("Dates are sorted in ascending order by date");
        } else {
            test.fail("Dates are not sorted in ascending order by date");
        }
    }

    public List<String> getDatesList(){
        List<WebElement> list=driver.findElements(myVieweingActivityDates);
        List<String> dates=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            dates.add(list.get(i).getText());
        }
        test.info("Publications Dates#"+dates);
        return dates;
    }

    public List<String> getPagesList(){
        List<WebElement> list=driver.findElements(myVieweingActivityPages);
        List<String> pages=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            pages.add(list.get(i).getText());
        }
        test.info("Publications Pages#"+pages);
        return pages;
    }

    public List<String> getSectionsList(){
        List<WebElement> list=driver.findElements(myVieweingActivitySections);
        List<String> sections=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            sections.add(list.get(i).getText());
        }
        test.info("Publications Sections#"+sections);
        return sections;
    }


    public void selectMenuBarList(String menuListItem){
        try {
            waitForElementToDisplay(menubarlist, 30);
            List<WebElement> list=driver.findElements(menubarlist);
            org.testng.Assert.assertTrue(list.size()>0);
            for(int i=0;i<list.size();i++){
                if(list.get(i).getText().equalsIgnoreCase(menuListItem)){
                    click(list.get(i));
                    test.info("Selected Menu list#"+menuListItem);
                    break;
                }
            }
        } catch (Exception e) {
            test.fail("Menu list item not Selected"+menuListItem);
        }
    }

    public void itemList(String item){
        try {
            List<WebElement> list=driver.findElements(navigationItemList);
            org.testng.Assert.assertTrue(list.size()>0);
            for(int i=0;i<list.size();i++){
                if(list.get(i).getAttribute("innerHTML").equalsIgnoreCase(item)){
                    click(list.get(i));
                    test.info("Selected Item#"+item);
                    break;
                }
            }
        } catch (Exception e) {
            test.fail("Navigation item not Selected"+item);
        }
    }


    public void waitForPageLoad(String item) throws Exception {
        try {
            waitUntilElementIsInvisible(loadingElement, 60);
            navigationItemElement = By.xpath("//h1[@class='section-index-page-title'][contains(text(),'"+item+"')]");
            System.out.println(getTextFromElement(navigationItemElement));
            waitUntilElementIsVisible(navigationItemElement, 20);
            test.pass("Navigation Item is loaded#"+getTextFromElement(navigationItemElement));
        } catch (Exception e) {
            test.fail("Navigation Item not loaded#"+getTextFromElement(navigationItemElement));
        }
    }

    public void verifyTextualInfo() throws Exception {
        try {
            waitUntilElementIsVisible(textInfo, 20);
            org.testng.Assert.assertTrue(getTextFromElement(textInfo).contains("USP-NF Online"));
            test.pass("Textual Information is not present on the Dashboard page");
        } catch (Exception e) {
            test.fail("Textual Information is not present on the Dashboard page");
        }
    }

    public void verifyRelatedPublicationSection() throws Exception {
        try {
            assert isElementCurrentlyDisplayed(bookmarkSectionColumnName);
            test.pass("Publication Section is displayed");
        } catch (Exception e) {
            test.fail("Publication Section is not displayed");
        }
    }

    public void verifyDateInBookmarksSection() throws Exception {
        try {
            assert isElementCurrentlyDisplayed(bookmarkDateColumnName);
            test.pass("Publication Date is displayed");
        } catch (Exception e) {
            test.fail("Publication Date is not displayed");
        }
    }

    public void verifyMenuBarListItem(String item) {
        try {
            waitForElementToDisplay(menubarlist, 5);
            List<WebElement> list=driver.findElements(menubarlist);
            List<String> listValues=new ArrayList<>();
            org.testng.Assert.assertTrue(list.size()>0);
            for(int i=0;i<list.size();i++) {
                listValues.add(list.get(i).getText());
            }
            org.testng.Assert.assertTrue(listValues.contains(item));
            test.info("Menu Item is displayed#"+item);
        } catch (Exception e) {
            test.fail("Menu Item is not displayed#"+item);
        }
    }

    public void verifyNavigationItem(String item) {
        try {
            waitForElementToDisplay(menubarlist, 5);
            List<WebElement> list=driver.findElements(navigationItemList);
            List<String> listValues=new ArrayList<>();
            org.testng.Assert.assertTrue(list.size()>0);
            for(int i=0;i<list.size();i++) {
                listValues.add(list.get(i).getText());
            }
            org.testng.Assert.assertTrue(listValues.contains(item));
            test.pass("Navigation Item is displayed#"+item);
        } catch (Exception e) {
            test.fail("Navigation Item is not displayed#"+item);
        }
    }

    public void sortByPages() throws Exception {
        moveToElement(pageSort);
        actionClick(pageSort);
        test.pass("Page column is sorted");
    }

    public void sortByDate() throws Exception {
        moveToElement(dateSort);
        actionClick(dateSort);
        test.pass("Date column is sorted");
    }

    public void sortBySection() throws Exception {
        moveToElement(sectionSort);
        actionClick(sectionSort);
        test.pass("Section column is sorted");
    }

    public void verifySectionsSorted(){
        if(isCollectionSorted(getSectionsList())) {
            test.pass("Sections are Displayed in Sorted order");
        }else{
            test.fail("Sections are not displayed in Sorted order");
        }
    }

    public void verifyPagesSorted(){
        if(isCollectionSorted(getPagesList())) {
            test.pass("Pages are Displayed in Sorted order");
        }else{
            test.fail("Pages are not Displayed in Sorted order");
        }
    }
}
