package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import stepDefinations.TestBase;

import java.util.List;
import java.util.Set;

public class ViewAccountInfoPage extends TestBase {
    By accountInfoLabel;
    By editMail;
    By passwordTab;
    By yourProfileTab;
    By managePasswordTab;
    By newPassword;
    By confirmPassword;
    By PassChangeconfirmMessage;
    By changePasswordButton;
    By userMenuLink;
    By manageSubscriptionMenuItem;
    By freeResourcesTabId;
    By paidResourcesTabId;
    By paidResourcesTab;
    By editSubscriptionKey;
    By errorMessage;
    By submitBtn;

    public ViewAccountInfoPage() {
        InitElements();
    }

    private void InitElements() {
        accountInfoLabel = By.id("site-name");
        editMail = By.id("mail");
        passwordTab = By.id("managePasswordTabId");
        yourProfileTab = By.xpath("//div[@id='mainArea']//h2[text()='Your Profile']");
        managePasswordTab = By.xpath("//div[@id='mainArea']//h2[text()='Manage Password']");
        newPassword = By.id("password1");
        confirmPassword = By.id("password2");
        PassChangeconfirmMessage = By.id("message");
        changePasswordButton = By.id("password_button");
        userMenuLink = By.id("user-menu");
        manageSubscriptionMenuItem = By.id("manageSubscriptionMenuItem");
        freeResourcesTabId = By.id("freeResourcesTabId");
        paidResourcesTabId = By.id("paidResourcesTabId");
        paidResourcesTab = By.xpath("//div[@id='mainArea']//h2[text()='Paid Resources']");
        editSubscriptionKey = By.id("subscriptionkey");
        errorMessage = By.id("message");
        submitBtn =By.id("submitBtn");
    }

    public void verifyAccountInformation() throws Exception {
        String parentWindow = driver.getWindowHandle();
        world.put("mainWin", parentWindow);
        Set<String> handles =  driver.getWindowHandles();
        System.out.println(handles);
        for(String windowHandle  : handles)
        {
            if(!windowHandle.equals(parentWindow))
            {
                driver.switchTo().window(windowHandle);
            }
        }
        assert waitForElementToDisplay(yourProfileTab, 20);
        assert isElementCurrentlyDisplayed(yourProfileTab);
        System.out.println("Account info Screen#"+getAttributeValueFromElement(accountInfoLabel,"aria-label"));
    }

    public void verifyManagePassword() throws Exception {
        assert waitForElementToDisplay(managePasswordTab, 20);
        assert isElementCurrentlyDisplayed(managePasswordTab);
    }

    public void verifyEmailNotEditable() throws Exception {
        assert isAttribtuePresent(editMail, "readonly");
        System.out.println("Account info Screen#"+getAttributeValueFromElement(accountInfoLabel,"aria-label"));
    }

    public void selectPasswordTab() throws Exception {
        assert click(passwordTab);
    }

    public void clickChangePasswordButton() throws Exception {
        assert click(changePasswordButton);
        assert waitForElementToDisplay(PassChangeconfirmMessage, 20);
    }

    public void changePassword(String randomPassword) {
        assert sendKeys(newPassword, randomPassword);
        assert sendKeys(confirmPassword, randomPassword);
    }

    public void verifyPasswordChangeconfirmMessage() throws Exception {
        Assert.assertTrue(getTextFromElement(PassChangeconfirmMessage).equalsIgnoreCase("Your USP Access Point account password has been updated successfully."));
    }

    public void selectManageSubscriptionMenu() throws Exception {
        moveToElement(userMenuLink);
        assert click(manageSubscriptionMenuItem);
    }

    public void verifyManageSubscriptionPageDisplayed() throws Exception {
        waitForElementToDisplay(freeResourcesTabId, 20);
        assert isElementCurrentlyDisplayed(freeResourcesTabId);
    }

    public void navigatePaidResourcesTab() throws Exception {
        assert click(paidResourcesTabId);
        waitForElementToDisplay(paidResourcesTab, 20);
        assert isElementCurrentlyDisplayed(paidResourcesTab);
    }

    public void selectSubscriptionKey(String subKey) throws Exception {
        assert sendKeys(editSubscriptionKey, subKey);
        assert click(submitBtn);
        pause(2000);
    }

    public void verifyErrorMessageSubscription(String msg) throws Exception {
        try {
            waitForElementToDisplay(errorMessage, 10);
            String actError=getTextFromElement(errorMessage);
            Assert.assertTrue(actError.toLowerCase().trim().contains(msg.toLowerCase().trim()));
            test.pass("Error message is displayed for invalid subscription key#"+actError);
        } catch (Exception e) {
            test.fail("Error message is not displayed for invalid subscription key#");
        }
    }


}
