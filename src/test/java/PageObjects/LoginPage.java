package PageObjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import stepDefinations.TestBase;

import java.io.IOException;
import java.util.List;

public class LoginPage extends TestBase {
    By editUserName;
    By editPassword;
    By buttonLogin;
    By labelHomePage;
    By dismissButton;
    By modelDialog;
    By welcomeLabel;
    By viewMyActivityLink;
    By errorMessage;
    By logBackInButton;
    By sessionExpiredHeader;
    By inactivityText;

    public LoginPage() {
        InitElements();
    }

    private void InitElements() {
        editUserName = By.id("username");
        editPassword = By.id("password");
        buttonLogin = By.name("btnSubmit");
        labelHomePage = By.xpath("//div[@class='dashboard-content-title']");
        dismissButton = By.xpath("//button[text()='Dismiss']");
        modelDialog = By.xpath("//div[@class='modal-dialog modal-dialog']");
        welcomeLabel = By.id("welcome");
        errorMessage = By.xpath("//div[@id='msg']//span");
        logBackInButton = By.xpath("//button[@class='session-expired-dialog-action session-expired-dialog-logout-button']");
        sessionExpiredHeader = By.xpath("//h1[@class='modal-dialog-header']");
        inactivityText = By.xpath("//div[@class='session-expired-dialog-content-html']//p//span");
    }

    public void enterUserNamePassword(String username, String password) throws Exception {
        try {
            world.put("username", username);
            world.put("password", password);
            sendKeys(editUserName, username);
            sendKeys(editPassword, password);
            test.info("Entered username#"+username);
        } catch (Exception e) {
            test.fail("Username not entered");
        }
    }

    public void clickSubmit() throws Exception {
        try {
            click(buttonLogin);
            test.info("Submit button clicked");
        } catch (Exception e) {
            test.fail("Login Failed");
        }
    }

    public void openBrowser() throws IOException {
        openDriverBrowser();
    }

    public void navigateToLoginPage() throws IOException {
        navigatetoLoginURL();
    }

    public void navigateToLoginPage(String url) throws IOException {
        navigatetoLoginURL(url);
    }

    public void verifyAccessPointLoginPage() throws Exception {
        try {
            waitForElementToDisplay(welcomeLabel, 60);
            org.testng.Assert.assertTrue(isElementCurrentlyDisplayed(welcomeLabel));
            test.pass("Access Point Login page is displayed");
        } catch (Exception e) {
            test.fail("Access Point Login page is not displayed");
        }
    }

    public void verifyUserNamePasswordFields() throws Exception {
        try {
            waitForElementToDisplay(welcomeLabel, 60);
            org.testng.Assert.assertTrue(isElementCurrentlyDisplayed(editUserName));
            org.testng.Assert.assertTrue(isElementCurrentlyDisplayed(editPassword));
            org.testng.Assert.assertTrue(isElementCurrentlyDisplayed(buttonLogin));
            test.pass("Login page is displayed");
        } catch (Exception e) {
            test.fail("Login page is not displayed");
        }
    }

    public void verifyErrorMessage(String msg) throws Exception {
        String actError = null;
        try {
            waitForElementToDisplay(errorMessage, 10);
            actError = getTextFromElement(errorMessage);
            Assert.assertTrue(actError.toLowerCase().trim().contains(msg.toLowerCase().trim()));
            test.pass("Error Message is displayed as # " + actError);
        } catch (Exception e) {
            test.fail("Error Message is displayed as # " + actError);
        }
    }

    public void verifyLogBackInButtonDisplayed() {
        try {
            assert isElementCurrentlyDisplayed(logBackInButton);
            test.pass("Log Back In button is Displayed");
        } catch (Exception e) {
            test.pass("Log Back In button is not displayed");
        }
    }

    public void selectLogBackIn(){
        try {
            assert click(logBackInButton);
            test.pass("Log Back In button is clicked");
        } catch (Exception e) {
            test.fail("Log Back In button is not clicked");
        }
    }

    public void pauseExecution(int waitTime) {
        pause(waitTime);
    }
}
