package runners;

import PageObjects.HomePage;
import PageObjects.LoginPage;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ExtentHtmlReporterConfiguration;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import libraries.ConfigReader;
import libraries.ReadEmails;
import org.testng.annotations.*;
import stepDefinations.TestBase;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


@CucumberOptions
        (features = "src/test/resources",
                plugin = {"pretty", "html:target/cucumber-html-report.html",
                        "html:target/cucumber-reports/cucumber-pretty.html",
                        "json:target/cucumber-reports/cucumber.json"},
//                        "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:target/html/ExtentReport.html"},
                glue = {"stepDefinations"},
//                tags="@RegressionTest")
                tags="@run")

public class TestRunner extends AbstractTestNGCucumberTests {
    public static ExtentTest logger;
    public static ExtentReports reports;
    public static ExtentTest test;
    static ExtentHtmlReporterConfiguration htmlReporter;
    static SimpleDateFormat format = null;
    static String reportName = null;
    static Calendar cal = null;
    static String environment = null;
    static String UserName = null;
    static  String Password = null;
    public String override=null;
    String reportTime;
    public Object[][] data;

    static {
        System.setProperty("log4j.configurationFile", "log4j2.xml");
    }

    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();

    public void reportSetup() {
        cal = Calendar.getInstance();
//        format = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss");
        TestBase.reports = new ExtentReports();
//        SimpleDateFormat formatt = new SimpleDateFormat("ddMMMyyyy_HH-mm");
//        reportTime = formatt.format(cal.getTime());
        TestBase.reportName = System.getProperty("user.dir") + "/Reports/USPNFExtentReport.html";
        TestBase.htmlReporter = new ExtentHtmlReporter(new File(TestBase.reportName));
        TestBase.htmlReporter.loadXMLConfig(String.valueOf(new File(System.getProperty("user.dir") + "/src/test/resources/extent-config.xml")));
        TestBase.reports.setSystemInfo("Environment",   TestBase.env);
        TestBase.reports.setSystemInfo("Browser",   TestBase.browser);
        TestBase.reports.setSystemInfo("Author", "Vijay Bompally");
        TestBase.reports.setSystemInfo("Executed By", System.getProperty("user.name"));
        TestBase.reports.setSystemInfo("Operating System", System.getProperty("os.name"));
        TestBase.reports.attachReporter( TestBase.htmlReporter);
    }

    @AfterClass
    public static void teardown() {
        System.out.println("Tear Down");
    }

    @AfterMethod
    public void closeBrowser() throws Exception {
        if (TestBase.driver == null) {
        } else {
            if(homePage.verifyUserAccountInfoLinkDisplayed()) {
                homePage.ExpandUserAccountInfoLink();
                homePage.logoutFromApplication();
                loginPage.verifyAccessPointLoginPage();
            }
            TestBase.driver.quit();
        }
    }

    @BeforeSuite
    public void setUp() throws IOException {
        String tempBrowser = System.getProperty("Browser");
        TestBase.browser = ConfigReader.getConfigValue("Browser");
        TestBase.expWait = Integer.parseInt(ConfigReader.getConfigValue("WaitTime"));
        if(tempBrowser != null && !tempBrowser.isEmpty()){
            TestBase.browser = tempBrowser;
        }
        System.out.println("Browser#"+TestBase.browser);

        String tempEnv = System.getProperty("env");
        TestBase.env = ConfigReader.getConfigValue("Env");
        if (tempEnv != null && !tempEnv.isEmpty()) {
            TestBase.env = tempEnv;
        }
        System.out.println("Environment#"+TestBase.env);

        String tempUserName = System.getProperty("UserName");
        TestBase.username = ConfigReader.getConfigValue("UserName_"+TestBase.env);
        if (tempUserName != null && !tempUserName.isEmpty()) {
            TestBase.username = tempUserName;
        }

        String tempPassword = System.getProperty("Password");
        TestBase.password = ConfigReader.getConfigValue("Password_"+TestBase.env);
        if (tempPassword != null && !tempPassword.isEmpty()) {
            TestBase.password = tempPassword;
        }
        reportSetup();
    }

    @AfterSuite
    public void SendReports() {
        try {
            ReadEmails.sendReportMail(TestBase.reportName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}