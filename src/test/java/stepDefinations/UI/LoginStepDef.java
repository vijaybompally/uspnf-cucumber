package stepDefinations.UI;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import libraries.ConfigReader;
import stepDefinations.TestBase;
import testData.TestDataGenerator;

import java.io.IOException;

public class LoginStepDef {
    LoginPage loginScreen = new LoginPage();
    private HomePage homePage=new HomePage();

    public LoginStepDef() throws IOException {
    }

    @When("I open login page")
    public void iOpenLoginPage() throws IOException {
        loginScreen.openBrowser();
        loginScreen.navigateToLoginPage();
    }

    @When("I navigate to login page")
    public void iNavigateLoginPage() throws IOException {
        loginScreen.navigateToLoginPage();
    }

    @Then("I verify Access Point Login page is displayed")
    public void iverifyAccessPointLoginpageisdisplayed() throws Exception {
        loginScreen.verifyAccessPointLoginPage();
    }

    @Then("I verify username and password fields")
    public void iVerifyUsernameAndPasswordFields() throws Exception {
        loginScreen.verifyUserNamePasswordFields();
    }

    @When("I enter valid username and password")
    public void iEnterUsernameAndPasswordAndClickOnLoginButton() throws Exception {
        loginScreen.enterUserNamePassword(TestBase.username, TestBase.password);
    }

    @And("I click on submit button")
    public void iClickOnSubmitButton() throws Exception {
        loginScreen.clickSubmit();
    }

    @Then("I should see recenty accessed documents")
    public void iShouldSeeRecentyAccessedDocuments() {

    }

    @When("I enter blank email and click on submit button")
    public void iEnterBlankEmailAndClickOnSubmitButton() throws Exception {
        loginScreen.enterUserNamePassword("", TestBase.password);
        loginScreen.clickSubmit();
    }

    @When("I enter blank password and click on submit button")
    public void iEnterBlankPasswordAndClickOnSubmitButton() throws Exception {
        loginScreen.enterUserNamePassword(TestBase.username, "");
        loginScreen.clickSubmit();
    }

    @When("I enter invalid email and click on submit button")
    public void iEnterInvalidEmailAndClickOnSubmitButton() throws Exception {
        loginScreen.enterUserNamePassword(TestBase.username+"invalid", TestBase.password);
        loginScreen.clickSubmit();
    }

    @When("I enter invalid password and click on submit button")
    public void iEnterInvalidPasswordAndClickOnSubmitButton() throws Exception {
        loginScreen.enterUserNamePassword(TestBase.username, TestBase.password+"invalid");
        loginScreen.clickSubmit();
    }

    @Then("error message is displayed as {string}")
    public void errorMessageIsDisplayedAs(String msg) throws Exception {
        loginScreen.verifyErrorMessage(msg);
    }

    @And("I login with valid credentials")
    public void iLoginWithValidCredentials() throws Exception {
        loginScreen.verifyAccessPointLoginPage();
        loginScreen.verifyUserNamePasswordFields();
        loginScreen.enterUserNamePassword(TestBase.username, TestBase.password);
        loginScreen.clickSubmit();
    }

    @When("I navigate to url {string}")
    public void iNavigateToUrl(String url) throws IOException {
        loginScreen.openBrowser();
        loginScreen.navigateToLoginPage(url);

    }

    @Then("NABU publication Dashboard is displayed")
    public void nabuPublicationDashboardIsDisplayed() throws Exception {
        homePage.verifyNabuHomePageDisplayed();
    }

    @When("I wait for {int} minutes for session Timeout")
    public void iWaitForMinutesForSessionTimeout(int waitTime) {
        loginScreen.pauseExecution(waitTime);
    }

    @Then("Log Back in button is displayed")
    public void logBackInButtonIsDisplayed() {
        loginScreen.verifyLogBackInButtonDisplayed();
    }

    @When("I click on Log Back In button")
    public void iClickOnLogBackInButton() {
        loginScreen.selectLogBackIn();
    }
}
