package stepDefinations.UI;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinations.TestBase;

import java.io.IOException;

public class IPStepDef {
    LoginPage loginScreen = new LoginPage();
    private HomePage homePage=new HomePage();

    public IPStepDef() throws IOException {
    }

    @When("I enter username and password and click on Login button")
    public void iEnterUsernameAndPasswordAndClickOnLoginButton() {
    }

    @Then("home page is displayed")
    public void homePageIsDisplayed() {
    }

    @When("I enter system will allow the user to navigate from the displayed document back to the relevant document grouping or sub-grouping.")
    public void iEnterSystemWillAllowTheUserToNavigateFromTheDisplayedDocumentBackToTheRelevantDocumentGroupingOrSubGrouping() {
    }

    @Then("System should allow navigating between sub groups")
    public void systemShouldAllowNavigatingBetweenSubGroups() {
    }

    @When("I enter breadcrumb will be linkable and allow the user to click on a section of the breadcrumb to access the relevant grouping or sub-grouping.")
    public void iEnterBreadcrumbWillBeLinkableAndAllowTheUserToClickOnASectionOfTheBreadcrumbToAccessTheRelevantGroupingOrSubGrouping() {
    }

    @Then("system should allow to navigate between grouping or sub-grouping")
    public void systemShouldAllowToNavigateBetweenGroupingOrSubGrouping() {
    }
}
