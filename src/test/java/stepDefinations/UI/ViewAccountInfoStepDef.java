package stepDefinations.UI;
import PageObjects.HomePage;
import PageObjects.ViewAccountInfoPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinations.TestBase;
import testData.TestDataGenerator;

import java.io.IOException;

public class ViewAccountInfoStepDef {
    ViewAccountInfoPage viewAccountInfoPage = new ViewAccountInfoPage();

    public ViewAccountInfoStepDef() throws IOException {
    }

    @Then("Account Information page is displayed")
    public void accountInformationPageIsDisplayed() throws Exception {
        viewAccountInfoPage.verifyAccountInformation();
    }

    @And("I verify email cannot be updated")
    public void iVerifyEmailCannotBeUpdated() throws Exception {
        viewAccountInfoPage.verifyEmailNotEditable();
    }

    @When("I navigate select password tab")
    public void iNavigateSelectPasswordTab() throws Exception {
        viewAccountInfoPage.selectPasswordTab();
    }

    @Then("Manage Password page is displayed")
    public void managePasswordPageIsDisplayed() throws Exception {
        viewAccountInfoPage.verifyManagePassword();
    }

    @When("I change password and click on change password button")
    public void iChangePasswordAndClickOnChangePasswordButton() throws Exception {
        for(int i=0;i<6;i++) {
            viewAccountInfoPage.changePassword(TestDataGenerator.getRandomPassword());
            viewAccountInfoPage.clickChangePasswordButton();
        }
        viewAccountInfoPage.changePassword(TestBase.password);
        viewAccountInfoPage.clickChangePasswordButton();
    }

    @Then("success message is displayed")
    public void successMessageIsDisplayed() throws Exception {
        viewAccountInfoPage.verifyPasswordChangeconfirmMessage();
    }

    @When("I navigate to Manage subscription menu")
    public void iNavigateToManageSubscriptionMenu() throws Exception {
        viewAccountInfoPage.selectManageSubscriptionMenu();
    }

    @Then("Manage subscriptions page is displayed")
    public void manageSubscriptionsPageIsDisplayed() throws Exception {
        viewAccountInfoPage.verifyManageSubscriptionPageDisplayed();
    }

    @When("I Enter subscription key as {string} and click add button")
    public void iEnterSubscriptionKeyAs(String subKey) throws Exception {
        viewAccountInfoPage.selectSubscriptionKey(subKey);
    }

    @When("I navigate to Paid Resources tab")
    public void iNavigateToPaidResourcesTab() throws Exception {
        viewAccountInfoPage.navigatePaidResourcesTab();
    }

    @Then("error message for subscription key is displayed as {string}")
    public void errorMessageForSubscriptionKeyIsDisplayedAs(String msg) throws Exception {
        viewAccountInfoPage.verifyErrorMessageSubscription(msg);
    }

    @When("I open {string} documents")
    public void iOpenDocuments(String arg0) {

    }

    @Then("maximum {string} should be displayed in my vieweing activity section")
    public void maximumShouldBeDisplayedInMyVieweingActivitySection(String arg0) {
    }

    @Then("Dashboard should display maximum {string} Recently accessed pages in my vieweing activity section")
    public void dashboardShouldDisplayMaximumRecentlyAccessedPagesInMyVieweingActivitySection(String arg0) {
    }
}
