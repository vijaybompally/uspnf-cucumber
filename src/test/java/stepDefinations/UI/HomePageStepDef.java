package stepDefinations.UI;
import PageObjects.HomePage;
import PageObjects.LoginPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepDefinations.TestBase;

import java.io.IOException;
import java.text.ParseException;

public class HomePageStepDef {
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();

    public HomePageStepDef() throws IOException {
    }

    @Then("USP-NF Home page is displayed")
    public void uspNFHomePageIsDisplayed() throws Exception {
        homePage.closeNoticeDialogs();
        homePage.verifyHomePageDisplayed();
    }

    @When("I navigate to MY VIEWING ACTIVITY section")
    public void iNavigateToMYVIEWINGACTIVITYSection() throws Exception {
        homePage.setViewMyActivityLink();
    }

    @When("I navigate to view Account Info section")
    public void iNavigateToViewAccountInfoSection() throws Exception {
        homePage.ExpandUserAccountInfoLink();
        homePage.navigateToViewAccountInfoSection();
    }

    @And("I logout from the application")
    public void iLogoutFromTheApplication() throws Exception {
        homePage.ExpandUserAccountInfoLink();
        homePage.logoutFromApplication();
        loginPage.verifyAccessPointLoginPage();
    }

    @When("I navigate to USP-NF Home page")
    public void iNavigateToUSPNFHomePage() {
        homePage.switcToHomePage();
    }

    @And("USP-NF Dashboard displays the Official current version")
    public void uspNFDashboardDisplaysTheOfficialCurrentVersion() throws Exception {
        homePage.verifyOfficialCurrentVersionLabel();
    }

    @And("My viewing activity section is displayed")
    public void myViewingActivitySectionIsDisplayed() throws Exception {
        homePage.verifyMyViewingActivitySection();
    }

    @And("Recently accessed publications displays in sorted descending order by date")
    public void recentlyAccessedPublicationsDisplaysInSortedDescendingOrderByDate() throws ParseException, InterruptedException {
        homePage.verifyPublicationsSortedByDescendingOrder();
    }

    @And("Recently accessed publications displays in sorted ascending order by date")
    public void recentlyAccessedPublicationsDisplaysInSortedAscOrderByDate() throws ParseException, InterruptedException {
        homePage.verifyPublicationsSortedByAscOrder();;
    }

    @When("I sort records by date")
    public void iSortRecordsByDate() throws Exception {
        homePage.sortByDate();
    }

    @When("I sort records by page")
    public void iSortRecordsByPage() throws Exception {
        homePage.sortByPages();
    }

    @Then("activity records are sorted by page")
    public void activityRecordsAreSortedByPage() {
        homePage.verifyPagesSorted();
    }

    @When("I sort records by section")
    public void iSortRecordsBySection() throws Exception {
        homePage.sortBySection();
    }

    @Then("activity records are sorted by section")
    public void activityRecordsAreSortedBySection() {
        homePage.verifySectionsSorted();
    }

    @When("I navigate away and returns to dashboard page")
    public void iNavigateAwayAndReturnsToDashboardPage() {

    }

    @When("I logout and open login page")
    public void iLogoutAndOpenLoginPage() throws Exception {
        homePage.ExpandUserAccountInfoLink();
        homePage.logoutFromApplication();
        loginPage.verifyAccessPointLoginPage();
    }

    @When("I click on Bookmarks tab")
    public void iClickOnBookmarksTab() throws Exception {
        homePage.clickBookmarksTab();
    }

    @Then("bookmarks displays in sorted descending order by date")
    public void bookmarksDisplaysInSortedDescendingOrderByDate() throws ParseException, InterruptedException {
        homePage.verifyPublicationsSortedByDescendingOrder();
    }

    @Then("bookmarks displays in sorted ascending order by date")
    public void bookmarksDisplaysInSortedAscOrderByDate() throws ParseException, InterruptedException {
        homePage.verifyPublicationsSortedByAscOrder();;
    }

    @And("menu bar list item {string} is displayed")
    public void menuBarListItemIsDisplayed(String item) {
        homePage.verifyMenuBarListItem(item);
    }

    @When("I select menu bar {string}")
    public void iSelectMenuBar(String menu) {
        homePage.selectMenuBarList(menu);
    }

    @Then("list item {string} is displayed")
    public void listItemIsDisplayed(String item) {
        homePage.verifyNavigationItem(item);
    }

    @And("I close menu bar {string}")
    public void iCloseMenuBar(String menu) {
        homePage.selectMenuBarList(menu);
    }

    @And("I select menubar {string} and select navigation item {string}")
    public void iSelectMenubarAndSelectNavigationItem(String menubar, String item) throws Exception {
        homePage.selectMenuBarList(menubar);
        homePage.itemList(item);
        homePage.waitForPageLoad(item);
    }

    @And("I verify textual information regarding USP-NF")
    public void iVerifyTextualInformationRegardingUSPNF() throws Exception {
        homePage.verifyTextualInfo();
    }

    @Then("bookmarks are displayed saved with date and related publication section in addition to associated links")
    public void bookmarksAreDisplayedSavedWithDateAndRelatedPublicationSectionInAdditionToAssociatedLinks() throws Exception {
        homePage.verifyDateInBookmarksSection();
        homePage.verifyRelatedPublicationSection();
    }


    @And("New links are displayed explaining page features")
    public void newLinksAreDisplayedExplainingPageFeatures() {

    }
}
