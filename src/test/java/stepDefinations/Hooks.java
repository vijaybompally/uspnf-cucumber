package stepDefinations;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import java.io.IOException;
import java.time.Instant;

public class Hooks extends TestBase{
    public static String scenarioName;

    public static String TestStatus = null;

    Instant before;
    Instant after;
    public static Instant beforetimestamp = Instant.now();

    @Before
    public void beforeScenario(Scenario scenario) {
        scenarioName = scenario.getName();
        test=reports.createTest("Scenario Name:"+scenario.getName());
    }

    @After
    public void closeExtentReport(Scenario scenario){
        if(scenario.getStatus().toString().equalsIgnoreCase("Passed")){
            test.pass("Scenario Passed");
        }else{
            test.fail("Scenario Failed");
        }
        reports.flush();
    }

    //    @AfterStep
    public void captureTimeAfter(Scenario scenario) {
        after = Instant.now();
    }

    //    @BeforeStep
    public void captureTimeBefore(Scenario scenario) {
        before = Instant.now();
    }

    @AfterStep
    public  void afterScenario(Scenario scenario) throws IOException {
        if (scenario.isFailed()) {
            if (driver != null) {
                try {
                    pause(2000);
                    TakesScreenshot ts = (TakesScreenshot) driver;
                    byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                    scenario.attach(screenshot, "image/png", "Screenshot");
                } catch (WebDriverException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}