package stepDefinations;

import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class sample {

    private SimpleDateFormat formatt;

    @Test
    public void test123() {
         System.out.println(getFirstDayOfWeek());
        System.out.println(getLastDayOfWeek());
    }

    public String getFirstDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        formatt=new SimpleDateFormat(("yyyy-MM-dd"));
      return formatt.format(calendar.getTime());
    }

    public String getLastDayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, +1);
        }
        formatt=new SimpleDateFormat(("yyyy-MM-dd"));
        return formatt.format(calendar.getTime());
    }
}

