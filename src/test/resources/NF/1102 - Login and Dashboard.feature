@RegressionTest
Feature:1102 - Login and Dashboard

  Scenario: 1102 - Login and Dashboard
    When I open login page
    Then I verify Access Point Login page is displayed
    And I verify username and password fields
    When I enter valid username and password
    And I click on submit button
    Then USP-NF Home page is displayed
    And I verify textual information regarding USP-NF
    And My viewing activity section is displayed
    And Recently accessed publications displays in sorted descending order by date