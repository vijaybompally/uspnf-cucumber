@RegressionTest
Feature: 0003 - Login - Access USP-NF through browser bookmarks

  Scenario: 0003 - Login - Access USP-NF through browser bookmarks
    When I open login page
    Then I verify Access Point Login page is displayed
    And I verify username and password fields
    When I enter invalid email and click on submit button
    Then error message is displayed as "Login credentials do not match our records. Please re-enter. If you continue to see this message, visit the links below to reset your password or create a new account."
    And I verify Access Point Login page is displayed