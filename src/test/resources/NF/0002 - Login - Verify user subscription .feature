@RegressionTest
Feature: 0002 - Login - Verify user subscription

  Scenario: 0002 - Login - Verify user subscription
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    When I navigate to view Account Info section
    Then Account Information page is displayed
    When I navigate to Manage subscription menu
    Then Manage subscriptions page is displayed
    When I navigate to Paid Resources tab
    And I Enter subscription key as "InvalidKEY" and click add button
    Then error message for subscription key is displayed as "The subscription key entered is not valid."
    When I Enter subscription key as "InvalidKEYInvalidKEY" and click add button
    Then error message for subscription key is displayed as "The subscription key entered is not valid."
    And I navigate to USP-NF Home page