@RegressionTest
Feature: 1306 - Bookmarks

  Scenario: 1306 - Bookmarks
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And My viewing activity section is displayed
    When I click on Bookmarks tab
    Then bookmarks are displayed saved with date and related publication section in addition to associated links.
    And bookmarks displays in sorted descending order by date
    When I sort records by date
    Then activity records are displayed by date
    When I sort records by page
    Then activity records are sorted by page
    When I sort records by section
    Then activity records are sorted by section
    When I navigate away and returns to dashboard page
    Then bookmarks displays in sorted descending order by date
    When I logout and open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And My viewing activity section is displayed
    When I click on Bookmarks tab
    Then bookmarks displays in sorted descending order by date
    And I logout from the application











