@RegressionTest
Feature:1103 -  Dashboard - Recently accessed publications

  Scenario:1103 -  Dashboard - Recently accessed publications
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And USP-NF Dashboard displays the Official current version
    And My viewing activity section is displayed
    And Recently accessed publications displays in sorted descending order by date
    When I sort records by date
    And Recently accessed publications displays in sorted ascending order by date
    When I sort records by page
    Then activity records are sorted by page
    When I sort records by section
    Then activity records are sorted by section
    When I open "11" documents
    Then Dashboard should display maximum "10" Recently accessed pages in my vieweing activity section
#    When I navigate away and returns to dashboard page
#    And Recently accessed publications displays in sorted descending order by date
    When I logout and open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And My viewing activity section is displayed
    And Recently accessed publications displays in sorted descending order by date
    And I logout from the application