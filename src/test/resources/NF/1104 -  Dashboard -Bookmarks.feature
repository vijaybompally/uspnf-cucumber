@RegressionTest
Feature:1104 -  Dashboard - Bookmarks

  Scenario:1104 -  Dashboard - Bookmarks
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And My viewing activity section is displayed
    When I click on Bookmarks tab
    Then bookmarks are displayed saved with date and related publication section in addition to associated links
    And bookmarks displays in sorted descending order by date
    When I sort records by date
    And bookmarks displays in sorted ascending order by date
    When I sort records by page
    Then activity records are sorted by page
    When I sort records by section
    Then activity records are sorted by section
    When I logout and open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And My viewing activity section is displayed
    When I click on Bookmarks tab
    Then bookmarks displays in sorted descending order by date












