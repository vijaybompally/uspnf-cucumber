@RegressionTest
Feature: 0001 - Login - Verify login functionality

  Scenario: 0001 - Login - Verify login functionality - Valid credentials
    When I open login page
    Then I verify Access Point Login page is displayed
    And I verify username and password fields
    When I enter valid username and password
    And I click on submit button
    Then USP-NF Home page is displayed

  Scenario: 0001 - Login - Verify login functionality - Invalid credentials
    When I open login page
    Then I verify Access Point Login page is displayed
    And I verify username and password fields
    When I enter blank email and click on submit button
    Then error message is displayed as "Username is a required field."
    When I enter blank password and click on submit button
    Then error message is displayed as "Password is a required field."
    When I enter invalid email and click on submit button
    Then error message is displayed as "Login credentials do not match our records. Please re-enter. If you continue to see this message, visit the links below to reset your password or create a new account."
    When I enter invalid password and click on submit button
    Then error message is displayed as "Login credentials do not match our records. Please re-enter. If you continue to see this message, visit the links below to reset your password or create a new account."

  Scenario: 0001 - Login - Verify login functionality - Change Email and Password
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    When I navigate to view Account Info section
    Then Account Information page is displayed
    And I verify email cannot be updated
    When I navigate select password tab
    Then Manage Password page is displayed
    When I change password and click on change password button
    Then success message is displayed
    When I navigate to USP-NF Home page

