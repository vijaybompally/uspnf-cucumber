
Feature:1203 -  Navigation
  Scenario:1203 -  Navigation


    When I open login page
    Then I verify username and password fields
    When I enter username and password and click on Login button
    Then home page is displayed
    When I  allow the user to navigate to the Dashboard from any page of the online publication
    Then system should allow to navigate from any page to Dashboard
    When I enter  system will, by default, navigate the user to the Currently Official version of the USP-NF Online when available

    And The publication version status "Currently Official", will display to the user with the published date, e.g. MMM DD, YYYY
    Then Default USP-NF Current Official version should be displayed
    And verified
    When I enter  the system will allow the user to navigate groupings and sub-groupings of documents relevant to that version. High-level document groupings may include the following.

 And Starting with USPNF 42, this navigational link, once clicked, will no longer display “Admissions” or “Annotated List” or “Front Matter”

 And General Notices
 And General Notices and Front Matter will display together under a document grouping.
 And NOTE – Exact name of document group is to be determined by the business.
    Then grouping and sub-grouping of document should be relevant to version
    When I enter the navigation link to USP resources will appear in the navigation menu.
    Then USP resources link should display in navigation menu
    When I enter Verify system will allow the user to navigate a table of contents for each document type relevant to the version selected by the user.

 And  High-level document types may include the following:
    And 1. Start Here
 And -New and Changed
 And -Admissions and Annotated Lists
 And -General Notices
 And -Front Matter
 And 2. General Chapters
 And -General Test & Assays
 And -General Information
 And -Dietary Supplements
 And -Chapter Chart
 And 3. Monograph
 And -USP
 And -NF
 And -Dietary Supplements
  And -Global Health
 And 4. Reagents and Reference Tables
  And -Reagents, Indicators, and Solutions
 And -Reagent Specifications
 And -Indicators and Indicator Test Papers
 And -Solutions
 And -Chromatographic Columns
 And -Reference Tables
 And 5. Resources :The system will support the display of instructional text or guidance related to each document type.
Then system should allow to navigate table of contents for each document
 And  Note: (Table of contents should be alphabetical list.)
    When I enter  the table of contents for each document type will provide a list of relevant documents.
    Then Table of contents should display the list of relevant documents











































