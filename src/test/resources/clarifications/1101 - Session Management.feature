#@RegressionTest
Feature: 1101 - Session Management

  Scenario: 1102 - Login and Dashboard
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    When I wait for 91 minutes for session Timeout
    Then Log Back in button is displayed
    When I click on Log Back In button
    Then I verify Access Point Login page is displayed
    When I enter valid username and password
    And I click on submit button
    Then USP-NF Home page is displayed
