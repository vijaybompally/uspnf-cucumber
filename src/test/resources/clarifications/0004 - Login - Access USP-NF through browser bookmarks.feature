#@RegressionTest
Feature: 0004 - Login - Access USP-NF through browser bookmarks

  Scenario: 0004 - Login - Access USP-NF through browser bookmarks
    When I navigate to url "http://publications.usp.org"
    And I login with valid credentials
    Then NABU publication Dashboard is displayed
