#@RegressionTest
Feature: 0004 - New feature on top of each page

  Scenario: 0004 - Login - Access USP-NF through browser bookmarks
    When I open login page
    And I login with valid credentials
    Then USP-NF Home page is displayed
    And New links are displayed explaining page features