
Feature:1205 - Navigation between Versions


  Scenario: 1205 - Navigation between Versions

    When I open login page
    Then I verify username and password fields
    When I enter username and password and click on Login button
    Then home page is displayed
    When I enter  the system, by default, will list the Currently Official documents to the user, if available.
    And When viewing the list within the Currently Official version, the documents will be tagged to express if the document is under revision.
    Then Currently Official document should be displayed. Revision should be tagged if document under revision
    When I enter currently official document will display the date on which it became official, for example “Official as of D-MMM-YYYY”
    And NOTE – Exact text to be determined by the business.
    And Note: If multiple official dates are in a single file, the system will generate the “official on" date based on the earliest future date available in the file.
    And Dates are generated in USP-NF based on the dates in the files delivered to NABU
    Then Document should display the latest official date delivered to NABU
    When I enter a document this is not yet official will not display an end date on which it is no longer official.
    Then currently official should not display the end date of the document
    When I enter currently official document that has a change, e.g. Revision Bulletin, will display text indicating that an updated version is available.
    And   NOTE – Exact text to be determined by the business.

    Then Revision Bulletin should be displayed
    When I enter a document that became official prior to 2013 and remains unchanged since that time (e.g. Monograph for Aspirin) will be identified with a "Currently Official" subtitle and marked as such, e.g. Official Prior to 2013”.
    Then Official prior to XXXX year should be displayed
    When I enter the system will allow the user to switch to the "No Longer Official" version
    Then system should allow to switch to Not Yet Official documents if applicable
    When I enter A document the is no longer official will be identified with a "No Longer Official "subtitle and display the date range in which it was official(with exception of documents the never become official), for example Official D-MMM-YYY to DMMM-YYYY"
    And  Note: Exact text to be determined by the business
    And  a document that become official prior to 2013 that becomes no longer official, will be identified with a "No Longer Official" Subtitle and will include the start and end date, e.g. "Official Prior to 2013 to D-MMM-YYYY".
    Then Verified the dates
    When I enter document that is not yet official will display the start date on which it will be official, for example “To be Official on D-MMM-YYYY”.” NOTE – Exact text to be determined by the business.
    And If multiple official dates are in a single file, the system will generate the “official on” date based on the earliest date available in the file.
    And Dates are generated in USP-NF based on the dates in the files delivered to NABU
    Then earliest start date on which it will be official should be displayed
    When I enter a document that is not yet official will not display an end date on which it is no longer official.
    Then end date which is no longer should not displayed
    When I enter Verify document that never becomes official (moves from "Not Yet Official“ to "No Longer official” will be identified with a "No Longer Official" subtitle and "Never Official".
    And  Note: These file will be viewable to the User
    Then document displays "Not Yet Official", "No Longer official" , "Never Official" and CURRENTLY OFFICIAL

    And  Verified files are viewable
    When I enter  user will be able to filter (select a facet) document lists and search results based on Official date of revision.
    Then Verified, faced is working based on revision
    When I enter  user will be able to filter (select a facet) document lists and search results based on publication.
    Then Verified, faced is working based on publication.
    When I enter   the system will allow the user to switch the list to the No Longer Official documents, if applicable.
    And NOTE – This will likely not be available until the Archive feature is made available,
    And possibly a future release. However, it is noted here to account for the design space and the documents accounting for the Archive
    Then system should allow to switch to No Longer Official documents if applicable
    When I enter the system will allow the user to switch to the "Not Yet Official" version.
    And The version status of "Not Yet Official" will display to the user with the publication date, e.g. MMM DD, YYYY.
    And NOTE – Exact text to be determined by the business.
    Then verified new date version displays
    When I enter a document that is not yet official will be identified with a "Not Yet Official" subtitle and display the start date on which it will be official , for example To be Official on D-MMM-YYYY.
    And NOTE – Exact text to be determined by the business.
    Then verified new date version displays
    When  the system will navigate the user to the document selected from the list.
    And 1. When viewing a document, the system will allow the user to switch to the Currently Official document, if available.


    And 2. When viewing a document, the system will allow the user to switch to the “Not Yet Official” document, if applicable.
    And 3. When viewing a document, the system will allow the user to switch to the “No Longer Official” document, if applicable.
    And NOTE – This will likely not be available until the Archive feature is made available, possibly a future release. However, it is noted here to account for the design space and the documents accounting for the Archive.

    And 4. The system will support the display of guidance or other instructions if the document no longer exists.
    Then Selected document should be displayed
    When I enter  the system will allow the user to navigate to a specific section within any accessed document
    Then system should allow to navigate to any section of the document

    When I enter the system will support links within a document to other referenced documents within the version accessed by the user.
    And The document will open in the same browser window although the user will have the option to use function keys to open in separate browser window.
    Then Revision will open in the same browser
    When I enter the system will support links in a document to the accelerated revisions as applicable.(See 2.3.3.4 for related requirement). The revision will open in the same browser window although the user will have the option to use function keys to open in a separate browser window.

    When I enter the system will support links within a document to external sources (e.g. USP public website pages, Pharmacopeial Forum. training course list, FAQs, Chromatograms. Impurity structures, etc.). The user will be able to view the external source(s) without losing the window to the product page currently being viewed.
    Then external source links should display in another window
    When I enter the system will allow the user to view a list of Reference Standards relevant to a Monograph.
    Then Reference Standard relevant to monograph should be displayed
    When I enter the system will allow the user to email a list of selected Reference Standards.
    And 1. The user will be able to initiate the email without losing the window to the product page currently being viewed.
    And 2. The system will allow the user to email a list with a single Reference Standard selected.
    And 3. The system will allow the user to email a list with multiple Reference Standards selected.
    And 4. The system will allow the user to email a list with all Reference Standards selected.
    And 5. The system will allow the user to cancel out of the email.
    And   6. The system will track analytics for each email event, capturing:
    And  The user who initiated the email
    And - The delivery email
    And  - The date on which the email went out
    And - The Monograph to which the email applies
    Then reference standard email should display in a separate window
    When I enter invalid username or password and click on Login button the system will support links to the USP iStore that display an individual Reference Standard relevant to a Monograph.
    Then reference standard link should navigate to iStore reference standard
    When I enter the system will support links within a document that initiate an email using the default email client (e.g. Outlook for USP). The user will be able to initiate and send the email without losing the window to the product page currently being viewed.
    Then reference standard email should display in a separate window
    When Verify the system will support links to the book references of the USP-NF.
    And NOTE: Previously part of requirement 2.5.4.
    Then reference standard links should be supported
    When I enter system will allow the user to navigate to the landing page of other USP online products to which they subscribe, e.g. FCC Online
    When  I enter official dates will not display in Front Matter.


























