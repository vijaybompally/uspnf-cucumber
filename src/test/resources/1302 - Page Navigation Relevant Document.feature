
Feature:1302 - Page Navigation Relevant Document
  Scenario: 1302 - Page Navigation Relevant Document


    When I open login page
    Then I verify username and password fields
    When I enter username and password and click on Login button
    Then home page is displayed
    When I enter system will allow the user to navigate from the displayed document back to the relevant document grouping or sub-grouping.
   Then System should allow navigating between sub groups
    When I enter breadcrumb will be linkable and allow the user to click on a section of the breadcrumb to access the relevant grouping or sub-grouping.
    Then system should allow to navigate between grouping or sub-grouping

