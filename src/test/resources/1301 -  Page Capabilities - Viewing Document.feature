
Feature:1301 -  Page Capabilities - Viewing Document
  Scenario: 1301 -  Page Capabilities - Viewing Document


    When I open login page
    Then I verify username and password fields
    When I enter username and password and click on Login button
    Then home page is displayed
    When I enter  logs into the application
    Then User is logged into application without any issues
When I enter  viewing a document, User should be able to see a color-coded banner
Then When viewing a document, User will see a color-coded banner displaying:
 And • Document version
 And • Date(s) associated with the document version
When I enter  the versions displayed on the documents
Then Displayed document versionswill include:
 And • “Currently Official”
 And • “To Be Official”
 And • “No Longer Official”
 And • “Never Official”

 And Note: User can select and open a document from the versions list.
When I viewing a document, User should be able to view supporting information related to that document
Then Document Tool will support:
 And • Text related to auxiliary information, e.g. contacts, expert committees, topics
 And • Links to external content
 And • Links to email, e.g. email address for the Scientific Liaison



