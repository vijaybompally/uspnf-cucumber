@RegressionTest @run
Feature: 1203 - Navigation

    Scenario:1203 - Navigation
        When I open login page
        And I login with valid credentials
        Then USP-NF Home page is displayed

        And menu bar list item "START HERE" is displayed
        And menu bar list item "GENERAL NOTICES" is displayed
        And menu bar list item "GENERAL CHAPTERS" is displayed
        And menu bar list item "MONOGRAPHS" is displayed
        And menu bar list item "REAGENTS AND REFERENCE TABLES" is displayed
        And menu bar list item "RESOURCES" is displayed

        When I select menu bar "START HERE"
        Then list item "New and Changed" is displayed
        Then list item "Admissions and Annotated Lists" is displayed
#        Then list item "General Notices" is displayed
        Then list item "Front Matter" is displayed
        And I close menu bar "START HERE"


        When I select menu bar "GENERAL NOTICES"
        Then list item "General Notices" is displayed
        And I close menu bar "GENERAL NOTICES"


        When I select menu bar "GENERAL CHAPTERS"
#        Then list item "General Tests & Assays" is displayed
        Then list item "General Information" is displayed
        Then list item "Dietary Supplements" is displayed
        Then list item "Chapter Charts" is displayed
        And I close menu bar "GENERAL CHAPTERS"

        When I select menu bar "MONOGRAPHS"
        Then list item "USP" is displayed
#        Then list item "NF" is displayed
        Then list item "Dietary Supplements" is displayed
        Then list item "Global Health" is displayed
        And I close menu bar "MONOGRAPHS"

        When I select menu bar "REAGENTS AND REFERENCE TABLES"
        Then list item "Reagents, Indicators, and Solutions" is displayed
        Then list item "Reagent Specifications" is displayed
        Then list item "Indicators and Indicator Test Papers" is displayed
        Then list item "Solutions" is displayed
        Then list item "Chromatographic Columns" is displayed
        Then list item "Reference Tables" is displayed
        And I close menu bar "REAGENTS AND REFERENCE TABLES"

        When I select menu bar "RESOURCES"
        And I close menu bar "RESOURCES"
        And I logout from the application



#    Verify system will allow the user to navigate to the Dashboard from any page of the online publication	system should allow to navigate from any page to Dashboard
#    "Verify the system will, by default, navigate the user to the Currently Official version of the USP-NF Online when available
#
#    The publication version status ""Currently Official"", will display to the user with the published date, e.g. MMM DD, YYYY"	"Default USP-NF Current Official version should be displayed
#
#
#    Verified "
#    "Verify the system will allow the user to navigate groupings and sub-groupings of documents relevant to that version. High-level document groupings may include the following.
#
#    Starting with USPNF 42, this navigational link, once clicked, will no longer display “Admissions” or “Annotated List” or “Front Matter”
#
#    General Notices
#    General Notices and Front Matter will display together under a document grouping.
#    NOTE – Exact name of document group is to be determined by the business."	grouping and sub-grouping of document should be relevant  to version
#    Verify the navigation link to USP resources will appear in the navigation menu.	USP resources link should display in navigation menu
#    "Verify system will allow the user to navigate a table of contents for each document type relevant to the version selected by the user.
#
#    High-level document types may include the following:
#
#    1. Start Here
#    -New and Changed
#    -Admissions and Annotated Lists
#    -General Notices
#    -Front Matter
#    2. General Chapters
#    -General Test & Assays
#    -General Information
#    -Dietary Supplements
#    -Chapter Chart
#    3. Monograph
#    -USP
#    -NF
#    -Dietary Supplements
#    -Global Health
#    4. Reagents and Reference Tables
#    -Reagents, Indicators, and Solutions
#    -Reagent Specifications
#    -Indicators and Indicator Test Papers
#    -Solutions
#    -Chromatographic Columns
#    -Reference Tables
#    5. Resources :The system will support the display of instructional text or guidance related to each document type."	"system should allow to navigate table of contents for each document
#    Note: (Table of contents should be alphabetical list.)"
#    Verify the table of contents for each document type will provide a list of relevant documents.	Table of contents should display the list of relevant documents
