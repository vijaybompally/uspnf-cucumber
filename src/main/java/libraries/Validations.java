package libraries;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Validations {
    public static Logger logger = LogManager.getLogger(Validations.class);

    public static boolean isTimeStampValid(String inputString) {
//        SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            format.parse(inputString);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static int compareDates(String first, String second) throws InterruptedException, ParseException {
        logger.info("comparing Dates#" + first + "#" + second);
        Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(first);
        Date end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(second);
        return start.compareTo(end);
    }

    public static int compareDate(String first, String second) throws InterruptedException, ParseException {
        logger.info("comparing Dates#" + first + "#" + second);
        Date start = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.ENGLISH).parse(first);
        Date end = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.ENGLISH).parse(second);
        return start.compareTo(end);
    }

    public static int compareAuditDates(String first, String second) throws InterruptedException, ParseException {
        logger.info("comparing Dates#" + first + "#" + second);
        Date start = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).parse(first);
        Date end = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.ENGLISH).parse(second);
        return start.compareTo(end);
    }
}
