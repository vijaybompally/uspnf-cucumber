package libraries;
import org.testng.annotations.Test;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

public class ReadEmails {
    public static Session readEmail() throws MessagingException, IOException {
        final String userid = "c-vijay.bompally@usp.org";
        final String password = "july2020july2020";
        Properties properties = new Properties();
        String code = null;
        properties.setProperty("mail.store.protocol", "imaps");
        properties.setProperty("mail.host", "smtp.office365.com");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", 587 );
        properties.put("mail.smtp.socketFactory.port", 587 );
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "true"); //changed from false to true for polestar
        properties.put("mail.pop3.starttls.enable", "true");

        Session session = Session.getDefaultInstance(properties,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(userid, password);
                    }
                });

        return session;
    }

    public static void sendReportMail(String reportName) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatt = new SimpleDateFormat("dd-MMM-yyyy_HH-mm");
        String reportTime = formatt.format(cal.getTime());
        String to = "c-vijay.bompally@usp.org";
        String subject = "USP NF Test Execution Report - " + reportTime;
        String messageContent = "Dear All, \n\nPlease find attached NF Execution report. \n\nRegards, \n Vijay";
        final String from = "c-vijay.bompally@usp.org";
        final String password = "july2020july2020";

        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.put("mail.debug", "false");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.office365.com");
        props.put("mail.smtp.port", 587);

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(from,password);
                    }
                });

        try {
            Transport transport = session.getTransport();
            InternetAddress addressFrom = new InternetAddress(from);

            MimeMessage message = new MimeMessage(session);
            message.setSender(addressFrom);
            message.setSubject(subject);
            message.setContent(messageContent, "html/plain");
            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));

            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart = new MimeBodyPart();
            Multipart multipart = new MimeMultipart();

            String filename=reportName;
            DataSource source = new FileDataSource(filename);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(filename);
            multipart.addBodyPart(messageBodyPart);
            message.setContent(multipart);
            transport.connect();
            Transport.send(message);
            transport.close();

        } catch (final MessagingException ex) {

        }
    }

    @Test
    public void test() {
        sendReportMail(System.getProperty("user.dir") + "/Reports/USPNFExtentReport.html");
    }
}

